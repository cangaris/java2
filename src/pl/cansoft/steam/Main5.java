package pl.cansoft.steam;

import java.util.List;
import java.util.stream.Collectors;

public class Main5 {

    public static void main(String[] args) {

        List<Integer> all = List.of(1,2,3,4,5,6,7,8,9);

        var result = all.stream()
                .map(input -> input * 3)
                .filter(input -> input % 2 == 0)
                .collect(Collectors.toList());

        System.out.println(result);
    }
}
