package pl.cansoft.steam;

import java.util.ArrayList;

public class Main2 {

    public static void main(String[] args) {

        var names = new ArrayList<String>();
        names.add("Damian");
        names.add("Kasia");
        names.add("Basia");
        names.add("Roman");
        names.add("Andrzej");

        var females = new ArrayList<String>();
        for(var name: names) {
            if (name.endsWith("a")) {
                females.add(name);
            }
        }
        System.out.println(females);
    }
}
