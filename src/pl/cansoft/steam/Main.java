package pl.cansoft.steam;

import java.util.ArrayList;
import java.util.function.Consumer;

public class Main {

    public static void main(String[] args) {

        var names = new ArrayList<String>();
        names.add("Damian");
        names.add("Kasia");
        names.add("Basia");
        names.add("Roman");
        names.add("Andrzej");

        // for each z lambdą (java 8+)
        names.forEach(s -> System.out.println(s));

        // for each z klasą anonimową (java 7-)
        names.forEach(
                new Consumer<String>() {
                    @Override
                    public void accept(String s) {
                        System.out.println(s);
                    }
                }
        );
    }
}
