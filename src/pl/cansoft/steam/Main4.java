package pl.cansoft.steam;

import java.util.ArrayList;
import java.util.Optional;

class User {
    String firstName;
    String lastName;
    Byte age;

    public User(String firstName, String lastName, Byte age) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.age = age;
    }

    @Override
    public String toString() {
        return "User{" +
                "firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", age=" + age +
                '}';
    }
}

public class Main4 {

    public static void main(String[] args) {

        var users = new ArrayList<User>();
        users.add(new User("Damian", "Kowalski", (byte) 31));
        users.add(new User("Jan", "Kowalski", (byte) 44));
        users.add(new User("Kornel", "Kowalski", (byte) 17));
        users.add(new User("Joanna", "Kowalska", (byte) 20));

        users.stream()
                .filter(user -> user.age < 18)
                .filter(user -> user.firstName.endsWith("a"))
                .findFirst()
                .ifPresentOrElse(
                        user -> {
                            System.out.println("Znalazłem użytkownika!");
                            System.out.println(user);
                        },
                        () -> System.out.println("Brak użytkownika!")
                );

        // User initialUser = new User("Damian", "Kowalski", (byte) 31);
        User initialUser = null;
        Optional<User> optionalUser = Optional.ofNullable(initialUser);
        User user = optionalUser.orElseThrow(() -> {
            throw new IllegalStateException("User musi istnieć!");
        });
        System.out.println(user);
    }
}
