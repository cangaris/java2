package pl.cansoft.steam;

import java.util.List;
import java.util.stream.Collectors;

public class Main7 {

    public static void main(String[] args) {

        List<String> names = List.of(
                "Damian", "Zosia", "Piotr",
                "@67454", "7eer###", "Ania",
                "1", "***K", "Gosia"
        );

        /**
         * https://regexr.com/ - stronka do wyrażeń regularnych
         */
        var result = names.stream()
                .filter(name -> name.matches("^[A-Z][a-z]+$"))
                .collect(Collectors.toList());

        System.out.println(result);
    }
}
