package pl.cansoft.steam;

import java.util.ArrayList;
import java.util.stream.Collectors;

public class Main3 {

    public static void main(String[] args) {

        var names = new ArrayList<String>();
        names.add("Damian");
        names.add("Kasia");
        names.add("Basia");
        names.add("Roman");
        names.add("Andrzej");

        var filtered = names.stream()
                .filter(name -> name.endsWith("a"))
                .map(name -> name.toUpperCase())
                .collect(Collectors.toList());

        System.out.println(filtered); // zmodyfikowana struktura
        System.out.println(names); // pierwotna - niezmieniona
    }
}
