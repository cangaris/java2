package pl.cansoft.steam;

import java.util.List;
import java.util.stream.Collectors;

public class Main8 {

    public static void main(String[] args) {

        List<String> names = List.of("Damian", "Ela", "Ania", "Gosia", "Zosia", "Piotr");

        List<String> namesFiltered = names.stream()
                .filter(name -> {
                    var code = (int) name.charAt(0); // String jest też tablicą char, odwołać się możemy do index 0 i po rzutowaniu na int mamy kod ASCI
                    return code >= 68 && code <= 71; // od D  do G (przedział)
                })
                .collect(Collectors.toList());

         System.out.println(namesFiltered);
    }
}
