package pl.cansoft.steam;

import java.util.List;
import java.util.stream.Collectors;

public class Main6 {

    public static void main(String[] args) {

        List<String> names = List.of("Damian", "Ania", "Gosia", "Piotr", "Darek", "Asia", "Dominik", "Dorian");

        var result = names.stream()
                .filter(name -> name.startsWith("D") || name.startsWith("A"))
                .filter(name -> name.endsWith("k") || name.endsWith("a"))
                .collect(Collectors.toList());

        System.out.println(result); // "Ania", "Darek", "Asia", "Dominik"
    }
}
