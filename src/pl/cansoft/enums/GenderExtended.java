package pl.cansoft.enums;

public enum GenderExtended {
    FEMALE("woman"),
    MALE("man");

    private String dbName;

    public String getDbName() {
        return this.dbName;
    }

    GenderExtended(String dbName) {
        this.dbName = dbName;
    }
}
