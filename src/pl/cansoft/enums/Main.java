package pl.cansoft.enums;

public class Main {

    public static void main(String[] args) {

        var femaleExtended = GenderExtended.FEMALE.getDbName();
        var maleExtended = GenderExtended.MALE;

        System.out.println(femaleExtended);
        System.out.println(maleExtended);

        var femaleSimple = GenderSimple.FEMALE;
        var maleSimple = GenderSimple.MALE;

        System.out.println(femaleSimple);
        System.out.println(maleSimple);
    }
}
