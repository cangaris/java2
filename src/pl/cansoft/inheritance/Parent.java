package pl.cansoft.inheritance;

public class Parent extends GrandParent {

    private String name;

    Parent(String name, String pesel) {
        super(pesel);
        this.name = name;
    }

    public void showName() {
        System.out.println("Wywołano z rodzica");
        System.out.println(name);
    }

    public void changeName(String name) {
        this.name = name;
    }
}
