package pl.cansoft.inheritance;

public class GrandParent {
    private String surname = "Cangaris";
    private String pesel;

    public GrandParent(String pesel) {
        this.pesel = pesel;
    }

    public void showSurname() {
        System.out.println(surname);
    }

    public void changeSurname(String surname) {
        this.surname = surname;
    }
}
