package pl.cansoft.inheritance;

public class Child1 extends Parent implements IHello {

    public Child1(String name, String pesel) {
        super(name, pesel);
    }

    @Override
    public void showName() {
        System.out.println("Wywołano z dziecka");
        super.showName();
    }
}
