package pl.cansoft.inheritance;

public class Main {

    public static void main(String[] args) {
        Child1 test1 = new Child1("Damian", "3789762873");
        test1.showName();
        test1.showSurname();
        test1.sayHello();
        test1.changeName("Jan");

        Child2 test2 = new Child2("Piotr", "87676786788");
        test2.showName();

        Child3 test3 = new Child3("Sławek", "212198655");
    }
}
