package pl.cansoft.inheritance;

public interface IHello {
    default void sayHello() {
        System.out.println("Hello!");
    }
}
