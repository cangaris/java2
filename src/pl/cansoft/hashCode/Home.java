package pl.cansoft.hashCode;

public class Home {

    private String street;
    private String postCode;
    private String city;
    private String country;

    public Home(String street, String postCode, String city, String country) {
        this.street = street;
        this.postCode = postCode;
        this.city = city;
        this.country = country;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getPostCode() {
        return postCode;
    }

    public void setPostCode(String postCode) {
        this.postCode = postCode;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Home home = (Home) o;

        if (!street.equals(home.street)) return false;
        if (!postCode.equals(home.postCode)) return false;
        if (!city.equals(home.city)) return false;
        return country.equals(home.country);
    }

    @Override
    public int hashCode() {
        int result = street.hashCode();
        result = 31 * result + postCode.hashCode();
        result = 31 * result + city.hashCode();
        result = 31 * result + country.hashCode();
        return result;
    }
}
