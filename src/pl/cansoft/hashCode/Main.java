package pl.cansoft.hashCode;

public class Main {

    public static void main(String[] args) {

        Home home = new Home("Marszałkowska", "50-550", "Warszawa", "PL");
        Home home2 = new Home("Marszałkowska", "50-550", "Warszawa", "PL");

        System.out.println( home );
        System.out.println( home2 );
        System.out.println( home.hashCode() );
        System.out.println( home2.hashCode() );
        System.out.println( home.equals(home2) );
    }
}
