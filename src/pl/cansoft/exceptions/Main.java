package pl.cansoft.exceptions;

public class Main {

    public static void main(String[] args) {
        try {
            // run1();
            // run2();
            run3();
        } catch (Exception e) {
            System.out.println("Ups! Coś poszło nie tak, " + e.getMessage());
        }
    }

    private static void run1() {
        throw new IllegalStateException("Stan aplikacji niedozwolony");
    }

    private static void run2() {
        throw new IllegalArgumentException("Argument niedozwolony");
    }

    private static void run3() {
        run4();
    }

    private static void run4() {
        run5();
    }

    private static void run5() {
        run6();
    }

    private static void run6() {
        try {
            throw new IllegalAccessException("Dostęp zabroniony");
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
    }
}
