package pl.cansoft.classes;

public class Home {

    String flatNr;
    int floors;
    int windows;
    static String m2 = "70m2";

    public Home(String flatNr, int floors, int windows) {
        this.flatNr = flatNr;
        this.floors = floors;
        this.windows = windows;
    }

    public Home(String flatNr, int floors) {
        this.flatNr = flatNr;
        this.floors = floors;
    }

    public Home(String flatNr) {
        this.flatNr = flatNr;
    }

    int getWindows() {
        return windows;
    }

    void setWindows(int windows) {
        this.windows = windows;
    }

    int getFloors() {
        return floors;
    }

    void setFloors(int floors) {
        this.floors = floors;
    }

    void setFlatNr(String flatNr) {
       this.flatNr = flatNr;
    }

    String getFlatNr() {
        return flatNr;
    }

    static void setM2(String m2) {
        Home.m2 = m2;
    }

    String getM2() {
        return m2;
    }

    @Override
    public String toString() {
        return "Home{" +
                "flatNr='" + flatNr + '\'' +
                ", windows=" + windows +
                ", floors=" + floors +
                '}';
    }
}
