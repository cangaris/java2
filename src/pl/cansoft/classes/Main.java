package pl.cansoft.classes;

public class Main {

    public static void main(String[] args) {

        Home home = new Home("1", 5, 115);
        Home home2 = new Home("2", 1, 14);
        Home home3 = new Home("3", 3, 15);
        Home home4 = new Home("4", 2, 10);
        Home home5 = new Home("15", 4, 15);

        home.setFlatNr("11");

        System.out.println(home);
        System.out.println(home2);
        System.out.println(home3);
        System.out.println(home4);
        System.out.println(home5);

        User user = new User("Damian", "Kowalski");
        user.setAge(44);
    }
}
