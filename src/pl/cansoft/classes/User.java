package pl.cansoft.classes;

@ShowText
public class User {

    @ShowText
    String firstName;
    String lastName;
    int age;

    User (String firstName, String lastName, int age) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.age = age;
    }

    User (String firstName, String lastName) {
        this.firstName = firstName;
        this.lastName = lastName;
    }

    User (String firstName) {
        this.firstName = firstName;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    @ShowText
    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }
}
