package pl.cansoft.animals;

public class Main {

    public static void main(String[] args) {

        Kot kot = new Kot();
        kot.jedz();

        System.out.println("------");

        Kura kura = new Kura();
        kura.jedz();
        kura.zniesJajko();

        System.out.println("------");

        Orzel orzel = new Orzel();
        orzel.lataj();
        orzel.zniesJajko();
        orzel.jedz();

        System.out.println("------");
    }
}
