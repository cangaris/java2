package pl.cansoft.interfaces;

public interface ITest {
    void printMessage();
    default void printHello() {
        System.out.println("Hello world");
    }
}
