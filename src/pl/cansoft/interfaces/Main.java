package pl.cansoft.interfaces;

public class Main {

    public static void main(String[] args) {
        Test1 test1 = new Test1();
        test1.printTitle();

        Test2 test2 = new Test2();
        Test3 test3 = new Test3();
        Test4 test4 = new Test4();

        showMessage(test1);
        showMessage(test2);
        showMessage(test3);
        showMessage(test4);
    }

    static void showMessage(ITest test) {
        test.printMessage();
        test.printHello();
    }
}
