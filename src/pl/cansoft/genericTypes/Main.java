package pl.cansoft.genericTypes;

import java.util.ArrayList;

class Orange {
}

class Apple {
}

class Box<T> {
    public T fruit;

    public Box(T fruit) {
        this.fruit = fruit;
    }

    public T getFruit() {
        return fruit;
    }
}

/**
 * ARRAY LIST
 * plusy: dostęp po indexach, update
 * minusy: dodawanie, usuwanie
 * {1,2,3,4,5,6,7,8,9,10}
 * [][][][x][x][x][][][][][][][][][x][x]
 * [1][2][3][4][5][66][7][8][9][10][][][]
 *
 * LINKED LIST
 * plusy: dodawanie, usuwanie
 * minusy: przeszukiwanie i dostęp do indexów, update
 * {1,2,3,4,5,6,7,8,9,10,11,12}
 * [1(#2)][2(#3)][3(#5)][x][x][x][][5][6][7][x][x][8][9][10][11(#10,#12)][x]
 * [x][x][x][x][x][x][x][x][x][12(#11)]
 */
public class Main {
    public static void main(String[] args) {

        Box<String> stringBox = new Box<String>("dada");
        Box<Apple> appleBox = new Box<Apple>(new Apple());
        Box<Orange> orangeBox = new Box<Orange>(new Orange());

        ArrayList<Integer> names = new ArrayList<>();
        names.add(1);
    }
}
