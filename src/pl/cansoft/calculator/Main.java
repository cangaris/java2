package pl.cansoft.calculator;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {

        Scanner inputScanner = new Scanner(System.in);

        System.out.println("Wybierz operację");
        System.out.println("Menu: [A] dodawanie, [B] odejowanie, [C] mnożenie, [D] dzielenie, [E] potęga, [F] pierwiastek");
        System.out.print("Wpisz opcję: ");

        String operation = inputScanner.nextLine().toUpperCase();

        System.out.println("Wybrana operacja: " + operation);

        switch (operation) {
            case "A":
                plus(inputScanner);
                break;
            case "B":
                minus(inputScanner);
                break;
            case "C":
                multiply(inputScanner);
                break;
            case "D":
                divide(inputScanner);
                break;
            case "E":
                pow(inputScanner);
                break;
            case "F":
                sqrt(inputScanner);
                break;
            default:
                throw new IllegalStateException("Nieznana operacja");
        }
    }

    static void sqrt(Scanner inputScanner) {

        System.out.print("Wpisz liczbę: ");
        var number = inputScanner.nextDouble();

        var result = Math.sqrt(number);
        System.out.println("Liczba wpisana: " + result);
    }

    static void pow(Scanner inputScanner) {

        System.out.print("Wpisz liczbę: ");
        var number = inputScanner.nextDouble();

        var result = Math.pow(number, 2);
        System.out.println("Liczba wpisana: " + result);
    }

    static void divide(Scanner inputScanner) {

        System.out.print("Wpisz liczbę nr 1: ");
        var number1 = inputScanner.nextDouble();

        System.out.print("Wpisz liczbę nr 2: ");
        var number2 = inputScanner.nextDouble();

        if (number2 == 0) {
            throw new IllegalStateException("Nie wolno dzielić przez zero");
        }

        System.out.println("Liczba wpisana: " + (number1 / number2));
    }

    static void multiply(Scanner inputScanner) {

        System.out.print("Wpisz liczbę nr 1: ");
        var number1 = inputScanner.nextDouble();

        System.out.print("Wpisz liczbę nr 2: ");
        var number2 = inputScanner.nextDouble();

        System.out.println("Liczba wpisana: " + (number1 * number2));
    }

    static void plus(Scanner inputScanner) {

        System.out.print("Wpisz liczbę nr 1: ");
        var number1 = inputScanner.nextDouble();

        System.out.print("Wpisz liczbę nr 2: ");
        var number2 = inputScanner.nextDouble();

        System.out.println("Liczba wpisana: " + (number1 + number2));
    }

    static void minus(Scanner inputScanner) {
        System.out.print("Wpisz liczbę nr 1: ");
        var number1 = inputScanner.nextDouble();

        System.out.print("Wpisz liczbę nr 2: ");
        var number2 = inputScanner.nextDouble();

        System.out.println("Liczba wpisana: " + (number1 - number2));
    }
}
